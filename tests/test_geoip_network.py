from ipaddress import IPv4Network, IPv6Network

import requests

import geoip_network

def test_lookup_ip_mocked(response_mock):
    with open("tests/mocks/198.51.100.1") as f:
        response = f.read()
    with response_mock(response):
        result = geoip_network.lookup_ip("198.51.100.1")
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")


def test_lookup_cidr_mocked(response_mock):
    with open("tests/mocks/198.51.100") as f:
        response = f.read()
    with response_mock(response):
        result = geoip_network.lookup_cidr("198.51.100.0/24")
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")


def test_lookup_bulk_mocked(response_mock):
    with open("tests/mocks/bulk") as f:
        response = f.read()
    with response_mock(response):
        results = geoip_network.lookup_bulk(["2001:db8::/32", "198.51.100.1", "0.0.0.0/24"])
        result = next(results)
        assert result.error is False
        assert result.cidr == IPv6Network("2001:db8::/32")
        result = next(results)
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")
        result = next(results)
        assert result.error is True


def test_lookup_cidr_session_mocked(response_mock):
    with open("tests/mocks/login_good") as f:
        login_response = f.read()
    with response_mock(login_response):
        geoip = geoip_network.GeoIP("username", "password")
    with open("tests/mocks/198.51.100") as f:
        response = f.read()
    with response_mock(response):
        result = geoip.lookup_cidr("198.51.100.0/24")
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")


def test_lookup_ip_session_mocked(response_mock):
    with open("tests/mocks/login_good") as f:
        login_response = f.read()
    with response_mock(login_response):
        geoip = geoip_network.GeoIP("username", "password")
    with open("tests/mocks/198.51.100.1") as f:
        response = f.read()
    with response_mock(response):
        result = geoip.lookup_ip("198.51.100.1")
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")


def test_lookup_bad_session_mocked(response_mock):
    with open("tests/mocks/login_bad") as f:
        login_response = f.read()
    with response_mock(login_response):
        try:
            geoip = geoip_network.GeoIP("username", "password")
        except requests.exceptions.HTTPError as e:
            assert e.response.status_code == 401
        else:
            assert False


def test_lookup_bulk_session_mocked(response_mock):
    with open("tests/mocks/login_good") as f:
        login_response = f.read()
    with response_mock(login_response):
        geoip = geoip_network.GeoIP("username", "password")
    with open("tests/mocks/bulk") as f:
        response = f.read()
    with response_mock(response):
        results = geoip.lookup_bulk(["2001:db8::/32", "198.51.100.1", "0.0.0.0/24"])
        result = next(results)
        assert result.error is False
        assert result.cidr == IPv6Network("2001:db8::/32")
        result = next(results)
        assert result.error is False
        assert result.cidr == IPv4Network("198.51.100.0/24")
        result = next(results)
        assert result.error is True
