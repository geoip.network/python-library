from ipaddress import IPv4Network, IPv6Network

from geoip_network.models import Result, Location


def test_result_from_dict_error():
    result = Result.from_dict(
        {'error': 'no covering prefix found'}
    )
    assert result.error is True


def test_result_to_dict_error():
    result = Result()
    result.error = True
    assert result.to_dict() == {'error': 'no covering prefix found'}


def test_result_from_dict_ipv4():
    result = Result.from_dict(
        {'allocated_cc': '--',
         'as-name': 'Documentation ASN',
         'asn': 'AS64496',
         'cidr': '198.51.100.0/24',
         'geo': {
             'geometry': {
                 'coordinates': [16.72425629, 62.88018421],
                 'type': 'Point'
             },
             'properties': {
                 'radius': 5.0
             },
             'type': 'Feature'
         },
         'rir': 'IANA',
         'timestamp': 1634593342
         }
    )
    assert result.error is False
    assert result.asn == 'AS64496'
    assert result.cidr == IPv4Network('198.51.100.0/24')
    assert result.timestamp == 1634593342
    assert result.rir == 'IANA'
    assert result.as_name == 'Documentation ASN'
    assert result.allocated_cc == '--'
    assert result.location.longitude == 16.72425629
    assert result.location.latitude == 62.88018421
    assert result.location.radius == 5.0


def test_result_from_dict_ipv6():
    result = Result.from_dict(
        {'allocated_cc': '--',
         'as-name': 'Documentation ASN',
         'asn': 'AS64496',
         'cidr': '2001:db8::/32',
         'geo': {
             'geometry': {
                 'coordinates': [16.72425629, 62.88018421],
                 'type': 'Point'
             },
             'properties': {
                 'radius': 5.0
             },
             'type': 'Feature'
         },
         'rir': 'IANA',
         'timestamp': 1634593342
         }
    )
    assert result.error is False
    assert result.asn == 'AS64496'
    assert result.cidr == IPv6Network('2001:db8::/32')
    assert result.timestamp == 1634593342
    assert result.rir == 'IANA'
    assert result.as_name == 'Documentation ASN'
    assert result.allocated_cc == '--'
    assert result.location.longitude == 16.72425629
    assert result.location.latitude == 62.88018421
    assert result.location.radius == 5.0


def test_result_to_dict_ipv6():
    result = Result()
    result.error = False
    result.asn = 'AS64496'
    result.cidr = IPv6Network('2001:db8::/32')
    result.timestamp = 1634593342
    result.rir = 'IANA'
    result.as_name = 'Documentation ASN'
    result.allocated_cc = '--'
    result.location = Location(16.72425629, 62.88018421, 5.0)
    assert result.to_dict() == {
        'allocated_cc': '--',
        'as-name': 'Documentation ASN',
        'asn': 'AS64496',
        'cidr': '2001:db8::/32',
        'geo': {
            'geometry': {
                'coordinates': [16.72425629, 62.88018421],
                'type': 'Point'
            },
            'properties': {
                'radius': 5.0
            },
            'type': 'Feature'
        },
        'rir': 'IANA',
        'timestamp': 1634593342
        }

def test_result_to_dict_ipv4():
    result = Result()
    result.error = False
    result.asn = 'AS64496'
    result.cidr = IPv4Network('198.51.100.0/24')
    result.timestamp = 1634593342
    result.rir = 'IANA'
    result.as_name = 'Documentation ASN'
    result.allocated_cc = '--'
    result.location = Location(16.72425629, 62.88018421, 5.0)
    assert result.to_dict() == {
        'allocated_cc': '--',
        'as-name': 'Documentation ASN',
        'asn': 'AS64496',
        'cidr': '198.51.100.0/24',
        'geo': {
            'geometry': {
                'coordinates': [16.72425629, 62.88018421],
                'type': 'Point'
            },
            'properties': {
                'radius': 5.0
            },
            'type': 'Feature'
        },
        'rir': 'IANA',
        'timestamp': 1634593342
        }

